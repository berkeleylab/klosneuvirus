### Supporting data for  

## Giant viruses with an extended complement of translation system components  
  
Frederik Schulz, Natalya Yutin, Natalia N. Ivanova, Davi R. Ortega, Tae Kwon Lee, Julia Vierheilig, Holger Daims, Matthias Horn, Michael Wagner, Grant J. Jensen, Nikos C. Kyrpides, Eugene V. Koonin, Tanja Woyke **Science  07 Apr 2017: Vol. 356, Issue 6333, pp. 82-85, DOI: 10.1126/science.aal4657**   

This repository contains all alignments, phylogenetic trees and the Count session file described in the paper. Please contact Frederik Schulz (fschulz@lbl.gov) for any questions regarding the data and their use.


Genomes and annotations of Klosneuviruses are available through this repository and soon in GenBank (accession numbers KY684083 to KY684123, submitted March 2017).
  
  
| Translation system component                      | Type | Abbrevation|
|---------------------------------------------------|------|------------|
| Isoleucine-tRNA synthetase                        | aaRS | IleRS      |
| Methionine-tRNA synthetase                        | aaRS | MetRS      |
| Glutamate-tRNA synthetase                         | aaRS | GluRS      |
| Aspartate-tRNA synthetase                         | aaRS | AspRS      |
| Alanyl-tRNA synthetase                            | aaRS | AlaRS      |
| Threonine-tRNA synthetase                         | aaRS | ThrRS      |
| Cysteinyl-tRNA synthetase                         | aaRS | CysRS      |
| Arginine-tRNA synthetase                          | aaRS | ArgRS      |
| Histidine-tRNA synthetase                         | aaRS | HisRS      |
| Proline-tRNA synthetase                           | aaRS | ProRS      |
| Lysine-tRNA synthetase                            | aaRS | LysRS      |
| Serine-tRNA synthetase                            | aaRS | SerRS      |
| Tyrosine-tRNA synthetase                          | aaRS | TyrRS      |
| Glycine-tRNA synthetase                           | aaRS | GlyRS      |
| Leucine-tRNA synthetase                           | aaRS | LeuRS      |
| Tryptophan-tRNA synthetase                        | aaRS | TrpRS      |
| Phenylalanine-tRNA synthetase                     | aaRS | PheRS      |
| Valyl-tRNA  synthetase                            | aaRS | ValRS      |
| Asparagine-tRNA synthetase                        | aaRS | AsnRS      |
| Glutamine-tRNA synthetase                         | aaRS | GlnRS      |
| Eukaryotic initiation factor 1                    | TF   | eIF-1      |
| Eukaryotic initiation factor 1A                   | TF   | eIF-1A     |
| Eukaryotic initiation factor 2 subunit alpha      | TF   | eIF-2a     |
| Eukaryotic initiation factor 2 subunit beta #1    | TF   | eIF-2b1    |
| Eukaryotic initiation factor 2 subunit beta #2    | TF   | eIF-2b2    |
| Eukaryotic initiation factor 2 subunit gamma      | TF   | eIF-2g     |
| Eukaryotic initiation factor 4A-III               | TF   | eIF-4A-III |
| Eukaryotic initiation factor 4E                   | TF   | eIF-4E     |
| Middle domain of eukaryotic initiation factor 4G  | TF   | eIF-4G     |
| Eukaryotic initiation factor 5A                   | TF   | eIF-5A     |
| Eukaryotic initiation factor 5B                   | TF   | eIF-5B     |
| Eukaryotic elongation factor 1                    | TF   | eEF-1      |
| Eukaryotic peptide chain release factor subunit 1 | TF   | eRF-1      |
| tRNA 2'-phosphotransferase 1                      | TM   | phospho    |
| tRNA pseudouridine synthase B                     | TM   | pseudo     |
| Peptidyl-tRNA hydrolase                           | TM   | peptidyl   |
| tRNA(Ile)-lysidine synthase                       | TM   | lysidine   |
| tRNA(His)-guanylyl transferase                    | TM   | guanyl     |